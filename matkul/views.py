from django.shortcuts import (get_object_or_404, 
                              render,  
                              HttpResponseRedirect) 

from .models import MatkulModel 
from .forms import MatkulForm

def create_matkul(request): 
    # dictionary for initial data with  
    # field names as keys 
    context ={} 
  
    # add the dictionary during initialization 
     
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            namaMatkul = request.POST['namaMatkul']+" berhasil ditambahkan"
            form.save()
            
            context = {'form': MatkulForm(), 'namaMatkul':namaMatkul}
            
            return render(request, "main/create_matkul.html", context)
    else:
        form = MatkulForm()      
    context['form']= form 
    return render(request, "main/create_matkul.html", context)

def list_matkul(request): 
    # dictionary for initial data with  
    # field names as keys 
    context ={} 
  
    # add the dictionary during initialization 
    context["dataset"] = MatkulModel.objects.all() 
          
    return render(request, "main/list_matkul.html", context) 

# pass id attribute from urls 
def detail_matkul(request, id): 
    # dictionary for initial data with  
    # field names as keys 
    context ={} 

    # add the dictionary during initialization 
    context["data"] = MatkulModel.objects.get(id = id) 
      
    return render(request, "main/detail_matkul.html", context)

# delete view for details 
def delete_matkul(request, id): 

    # dictionary for initial data with  
    # field names as keys 
    context ={} 
  
    # fetch the object related to passed id     
    obj = get_object_or_404(MatkulModel, id = id) 
    
    if request.method =="POST": 
        # delete object 
        obj.delete() 
        # after deleting redirect to  
        # home page 
        return HttpResponseRedirect("/matkul/list_matkul") 
  
    return render(request, "main/delete_matkul.html", context)