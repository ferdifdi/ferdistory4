from django import forms
from .models import MatkulModel

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MatkulModel
        fields = ['namaMatkul', 'dosen', 'sks', 'deskripsi', 'tahun', 'ruang']

        error_messages = {
            'required' : 'Please Type'
        }

        input_attrs_namaMatkul = {
            'type' : 'text',
            'placeholder' : 'Nama Matkul Anda'
        }

        input_attrs_dosen = {
            'type' : 'text',
            'placeholder' : 'Nama Dosen Pengampu'
        }

        input_attrs_sks = {
            'type' : 'text',
            'placeholder' : 'Jumlah SKS'
        }

        input_attrs_deskripsi = {
            'type' : 'text',
            'placeholder' : 'Deskripsi Matkul Anda'
            
        }

        input_attrs_tahun = {
            'type' : 'text',
            'placeholder' : 'contoh: Gasal 2019/2020'
        }

        input_attrs_ruang = {
            'type' : 'text',
            'placeholder' : 'Ruang Kelas'
        } 

        widgets = {
            'namaMatkul': forms.TextInput(attrs=input_attrs_namaMatkul),
            'dosen': forms.TextInput(attrs=input_attrs_dosen),
            'sks': forms.TextInput(attrs=input_attrs_sks),
            'deskripsi': forms.Textarea(attrs=input_attrs_deskripsi),
            'tahun': forms.TextInput(attrs=input_attrs_tahun),
            'ruang': forms.TextInput(attrs=input_attrs_ruang),
        }

        labels = {
            'namaMatkul': 'Nama Matkul',
            'dosen':      'Dosen',
            'sks': 'SKS',
            'deskripsi': 'Deskripsi',
            'tahun': 'Tahun',
            'ruang': 'Ruang',
        }