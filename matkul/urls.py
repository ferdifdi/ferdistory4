from django.urls import path
from django.conf.urls import url

from .views import  create_matkul, list_matkul, detail_matkul, delete_matkul

app_name = 'matkul'

urlpatterns = [
    
    path('create_matkul', create_matkul, name='create_matkul'),
    path('list_matkul', list_matkul, name='list_matkul'),
    path('<id>', detail_matkul ),
    path('<id>/delete', delete_matkul ),
    
]
