# Generated by Django 3.1.2 on 2020-10-16 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('matkul', '0004_auto_20201014_2247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkulmodel',
            name='deskripsi',
            field=models.TextField(max_length=200),
        ),
    ]
