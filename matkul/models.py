from django.db import models

# Create your models here.
class MatkulModel(models.Model):
    namaMatkul = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=3)
    deskripsi = models.TextField(max_length=200)
    tahun = models.CharField(max_length=15)
    ruang = models.CharField(max_length=15)

    def __str__(self):
        return self.namaMatkul