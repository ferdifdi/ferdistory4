from django import forms
from .models import KegiatanModel, PesertaKegiatanModel

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = KegiatanModel
        fields = ['namaKegiatan', 'deskripsi']

        error_messages = {
            'required' : 'Please Type'
        }

        input_attrs_namaKegiatan = {
            'type' : 'text',
            'placeholder' : 'Nama Kegiatan Anda'
        }

        input_attrs_deskripsi = {
            'type' : 'text',
            'placeholder' : 'Deskripsi Kegiatan Anda'   
        } 

        widgets = {
            'namaKegiatan': forms.TextInput(attrs=input_attrs_namaKegiatan),
            'deskripsi': forms.Textarea(attrs=input_attrs_deskripsi),
        }

        labels = {
            'namaKegiatan': 'Nama Kegiatan',
            'deskripsi': 'Deskripsi',
        }

class PesertaKegiatanForm(forms.ModelForm):
    class Meta:
        model = PesertaKegiatanModel
        fields = ['namaPeserta']

        error_messages = {
            'required' : 'Please Type'
        }

        input_attrs_namaPeserta = {
            'type' : 'text',
            'placeholder' : 'Nama Kegiatan Anda'
        }

        widgets = {
            'namaKegiatan': forms.TextInput(attrs=input_attrs_namaPeserta),
        }

        labels = {
            'namaPeserta': 'Nama Peserta',
        }