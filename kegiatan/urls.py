from django.urls import path
from django.conf.urls import url

from .views import  create_kegiatan, register_kegiatan, list_kegiatan, list_peserta

app_name = 'kegiatan'

urlpatterns = [
    
    path('create_kegiatan', create_kegiatan, name='create_kegiatan'),
    path('register_kegiatan/<id>', register_kegiatan, name='register_kegiatan'),
    path('list_peserta/<id>', list_peserta, name='list_peserta'),
    path('list_kegiatan', list_kegiatan, name='list_kegiatan'),
]
