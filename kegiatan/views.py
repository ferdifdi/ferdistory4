from django.shortcuts import (get_object_or_404, 
                              render,  
                              HttpResponseRedirect) 

from .models import KegiatanModel, PesertaKegiatanModel 
from .forms import KegiatanForm, PesertaKegiatanForm

def create_kegiatan(request): 
    # dictionary for initial data with  
    # field names as keys 
    context ={} 
  
    # add the dictionary during initialization 
     
    if request.method == "POST":
        form = KegiatanForm(request.POST)
        if form.is_valid():
            namaKegiatan = request.POST['namaKegiatan']+" berhasil ditambahkan"
            form.save()
            
            context = {'form': KegiatanForm(), 'namaKegiatan':namaKegiatan}
            
            return render(request, "main/create_kegiatan.html", context)
    else:
        form = KegiatanForm()      
    context['form']= form 
    return render(request, "main/create_kegiatan.html", context)

def register_kegiatan(request, id): 
    # dictionary for initial data with  
    # field names as keys 
    context ={} 
  
    # add the dictionary during initialization 
     
    if request.method == "POST":
        form = PesertaKegiatanForm(request.POST)
        if form.is_valid():
            namaPeserta = request.POST['namaPeserta']+" berhasil ditambahkan"
            obj = get_object_or_404(KegiatanModel, pk = id)
            form = PesertaKegiatanModel.objects.create(namaPeserta=request.POST['namaPeserta'], kegiatanmodel=obj)
            form.save()
            
            context = {'form': PesertaKegiatanForm(), 'namaPeserta':namaPeserta}
            
            return render(request, "main/register_kegiatan.html", context)
    else:
        form = PesertaKegiatanForm()      
    context['form']= form 
    return render(request, "main/register_kegiatan.html", context)    

def list_kegiatan(request): 
    # dictionary for initial data with  
    # field names as keys 
    context ={} 
  
    # add the dictionary during initialization 
    context["dataset"] = KegiatanModel.objects.all() 
          
    return render(request, "main/list_kegiatan.html", context) 

def list_peserta(request, id): 
    # dictionary for initial data with  
    # field names as keys 
    context ={} 
  
    # add the dictionary during initialization 
    context["dataset"] = PesertaKegiatanModel.objects.filter(kegiatanmodel__pk=id)
    return render(request, "main/list_peserta.html", context) 


