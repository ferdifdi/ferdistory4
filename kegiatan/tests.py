from django.test import TestCase, Client
from django.urls import resolve
from .views import create_kegiatan, register_kegiatan, list_kegiatan, list_peserta
from .models import KegiatanModel, PesertaKegiatanModel 
from .forms import KegiatanForm, PesertaKegiatanForm
from django.shortcuts import get_object_or_404

# Create your tests here.
class KegiatanTest(TestCase):        
    
# create_kegiatan
    # Test URL 
    def test_kegiatan_create_kegiatan_is_exist(self):  
        response = Client().get('/kegiatan/create_kegiatan')
        self.assertEquals(response.status_code, 200)
    
    # Test views
    def test_kegiatan_create_kegiatan_using_create_kegiatan_func(self):
        found = resolve('/kegiatan/create_kegiatan')
        self.assertEqual(found.func, create_kegiatan)
    
    # Test Database & Model
    def test_kegiatan_create_kegiatan_model(self):
        kegiatan = KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        hitungjumlah=KegiatanModel.objects.all().count()
        
        self.assertEqual(kegiatan.namaKegiatan, 'Kegiatan1')
        self.assertEqual(kegiatan.deskripsi, 'Deskripsi1')

        self.assertEqual(hitungjumlah,1)

    # Test forms & views
    def test_valid_data_dari_create_kegiatan(self):
        form = KegiatanForm({
            'namaKegiatan': "kegiatan1",
            'deskripsi': "deskripsi1",
        })
        self.assertTrue(form.is_valid())
        kegiatan = form.save()
        self.assertEqual(kegiatan.namaKegiatan, "kegiatan1")
        self.assertEqual(kegiatan.deskripsi, "deskripsi1")

    # Test HTML
    def test_kegiatan_create_kegiatan_using_create_kegiatan_template(self):
        response = Client().get('/kegiatan/create_kegiatan')
        self.assertTemplateUsed(response, 'main/create_kegiatan.html')
    
# register_kegiatan
    # Test URL
    def test_kegiatan_register_kegiatan_is_exist(self):
        KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        primary_key = KegiatanModel.objects.first()     
        response = Client().get(f'/kegiatan/register_kegiatan/{primary_key.id}')
        self.assertEquals(response.status_code, 200)
    
    # Test views
    def test_kegiatan_register_kegiatan_using_register_kegiatan_func(self):
        KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        primary_key = KegiatanModel.objects.first()   
        found = resolve(f'/kegiatan/register_kegiatan/{primary_key.id}')
        self.assertEqual(found.func, register_kegiatan)
    
    # Test Database & Model
    def test_kegiatan_register_kegiatan_model(self):
        k1 = KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        peserta = PesertaKegiatanModel.objects.create(namaPeserta='Peserta1', kegiatanmodel=k1)
        
        self.assertEqual(peserta.namaPeserta, 'Peserta1')
        self.assertEqual(peserta.kegiatanmodel, k1)

        hitungjumlah=PesertaKegiatanModel.objects.all().count()
        self.assertEqual(hitungjumlah,1)

    # Test forms & views
    # def test_valid_data_dari_register_kegiatan(self):
    #     k1 = KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
    #     form = PesertaKegiatanForm({
    #         'namaPeserta' : "Ferdi",
    #         'kegiatanmodel' : k1
    #     })
    #     self.assertTrue(form.is_valid())
    #     peserta = form.save()
    #     self.assertEqual(peserta.namaPeserta, "Ferdi")
    #     self.assertEqual(peserta.kegiatanmodel, k1)
        
    # Test HTML
    def test_kegiatan_create_kegiatan_using_create_kegiatan_template(self):
        response = Client().get('/kegiatan/create_kegiatan')
        self.assertTemplateUsed(response, 'main/create_kegiatan.html')

    # Test HTML
    def test_kegiatan_register_kegiatan_using_register_kegiatan_template(self):
        KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        primary_key = KegiatanModel.objects.first()   
        response = Client().get(f'/kegiatan/register_kegiatan/{primary_key.id}')
        self.assertTemplateUsed(response, 'main/register_kegiatan.html')

# list_kegiatan
    # Test URL 
    def test_kegiatan_list_kegiatan_is_exist(self):  
        response = Client().get('/kegiatan/list_kegiatan')
        self.assertEquals(response.status_code, 200)
    
    # Test views
    def test_kegiatan_list_kegiatan_using_list_kegiatan_func(self):
        found = resolve('/kegiatan/list_kegiatan')
        self.assertEqual(found.func, list_kegiatan)

    # Test HTML
    def test_kegiatan_list_kegiatan_using_list_kegiatan_template(self):
        response = Client().get('/kegiatan/list_kegiatan')
        self.assertTemplateUsed(response, 'main/list_kegiatan.html')    
    
# list_peserta
    # Test URL 
    def test_kegiatan_list_peserta_is_exist(self): 
        KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        primary_key = KegiatanModel.objects.first()   
        response = Client().get(f'/kegiatan/list_peserta/{primary_key.id}')
        self.assertEquals(response.status_code, 200)
    
    # Test views
    def test_kegiatan_list_peserta_using_list_peserta_func(self):
        KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        primary_key = KegiatanModel.objects.first()
        found = resolve(f'/kegiatan/list_peserta/{primary_key.id}')
        self.assertEqual(found.func, list_peserta)

    # Test HTML
    def test_kegiatan_list_peserta_using_list_peserta_template(self):
        KegiatanModel.objects.create(namaKegiatan='Kegiatan1', deskripsi='Deskripsi1')
        primary_key = KegiatanModel.objects.first()   
        response = Client().get(f'/kegiatan/list_peserta/{primary_key.id}')
        self.assertTemplateUsed(response, 'main/list_peserta.html')    
    



