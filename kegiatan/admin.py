from django.contrib import admin
from .models import KegiatanModel, PesertaKegiatanModel

# Register your models here.
admin.site.register(KegiatanModel)
admin.site.register(PesertaKegiatanModel)