from django.db import models

# Create your models here.
class KegiatanModel(models.Model):
    namaKegiatan = models.CharField(max_length=30)
    deskripsi = models.TextField(max_length=200)

    def __str__(self):
        return self.namaKegiatan

class PesertaKegiatanModel(models.Model):
    namaPeserta = models.CharField(max_length=30)
    kegiatanmodel = models.ForeignKey(KegiatanModel, on_delete=models.CASCADE)

    def __str__(self):
        return self.namaPeserta